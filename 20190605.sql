﻿-- script para crear campos multivaluados y compuestos

DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE b20190605;
USE b20190605;

-- PERSONAS CON VARIOS TELEFONOS

CREATE OR REPLACE TABLE personas (
  dni varchar(10),
  nombre varchar(50),
  PRIMARY KEY (dni)
);

INSERT INTO personas (dni, nombre)
  VALUES ('d1', 'n1'),
  ('d2', 'n2'),
  ('d3', 'n3');

-- telefonos sería un campo multivaluado y se representa en SQL como otra tabla distinta

CREATE OR REPLACE TABLE telefonos (
  dni varchar(10),
  numero varchar(12),
  PRIMARY KEY (dni, numero),
  CONSTRAINT fkTelefonosPersonas FOREIGN KEY (dni) REFERENCES personas (dni)
);

-- insertamos dos telefonos a la persona 1 y un telefono a la persona 2

INSERT INTO telefonos (dni, numero)
  VALUES ('d1', 't1'),
  ('d1', 't2'),
  ('d2', 't3');                   