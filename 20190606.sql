﻿-- script para crear campos multivaluados y compuestos

DROP DATABASE IF EXISTS b20190606;
CREATE DATABASE b20190606;
USE b20190606;

-- SOCIOS CON VARIOS TELEFONOS Y EMAILS QUE ALQUILAN PELICULAS EN VARIAS FECHAS

CREATE OR REPLACE TABLE socios (
  codigo int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY (codigo)
);

CREATE OR REPLACE TABLE telefonos (
  codSocio int,
  numero varchar(12),
  PRIMARY KEY (codSocio, numero),
  CONSTRAINT fkTelefonosSocios FOREIGN KEY (codSocio) REFERENCES socios (codigo)
);

CREATE OR REPLACE TABLE emails (
  codSocio int,
  email varchar(50),
  PRIMARY KEY (codSocio, email),
  CONSTRAINT fkEmailsSocio FOREIGN KEY (codSocio) REFERENCES socios (codigo)
);

CREATE OR REPLACE TABLE peliculas (
  codigo int AUTO_INCREMENT,
  titulo varchar(100),
  PRIMARY KEY (codigo)
);

CREATE OR REPLACE TABLE alquila (
  codSocio int,
  codPelicula int,
  PRIMARY KEY (codSocio, codPelicula),
  CONSTRAINT fkAlquilaSocio FOREIGN KEY (codSocio) REFERENCES socios (codigo),
  CONSTRAINT fkAlquilaPelicula FOREIGN KEY (codPelicula) REFERENCES peliculas (codigo)
);

CREATE OR REPLACE TABLE fechaAlquila (
  codSocio int,
  codPelicula int,
  fecha date,
  PRIMARY KEY (codSocio, codPelicula, fecha),
  CONSTRAINT fkFechaAlquilaAlquila FOREIGN KEY (codSocio, codPelicula) REFERENCES alquila (codSocio, codPelicula)
);
  